--	SELECT query basics

select * from Employee

-- SELECT query using GROUP BY clause
Select Count(*) as Count from Employee group by BandId

--SELECT query using HAVING clause
Select Count(*) as Count from Employee group by BandId Having BandId>1

--Using a Sub Query inside a SELECT query
select EmployeeId, EmployeeName,LocationId,BandId from Employee where BandId in (Select  Id from Band where id> 1)

--SELECT query using SELF JOIN
select emp1.EmployeeId, emp1.EmployeeName, emp2.LocationId, emp2.BandId from Employee emp1, Employee emp2 
where emp1.CurrentStatusId = emp2.CurrentStatusId

--SELECT query using LEFT OUTER JOIN
select Emp.EmployeeName, Emp.EmployeeId, Emp.LocationId, 
Loc.Code, Loc.StateName, Loc.Address from Employee Emp  left join Location Loc on Emp.LocationId= Loc.Id
--SELECT query using INNER JOIN
select Emp.EmployeeName, Emp.EmployeeId, Emp.LocationId, 
Loc.Code, Loc.StateName, Loc.Address from Employee Emp  inner join Location Loc on Emp.LocationId= Loc.Id
--INSERT statement � Insert values to a SQL table using INSERT script statement
insert into Employee (EmployeeId,EmployeeName,BandId,CurrentStatusId,LocationId ) values(275873, 'Prabha Srivastava', 3, 2, 5)
Select * from Employee
select Employee.LocationId from Employee group by LocationId 

--Using aggregate functions in a SELECT query
select COUNT(*) as Total_Employee_Count from Employee
select MAX(Employee.EmployeeId) from Employee

Select Id, Code,StateName from Location
Where Id = (Select Top(1) LocationId from Employee 
            Group By LocationId
            order by Count(LocationId) desc)

select * from Employee where LocationId=1

select count(*) from Employee where LocationId=1
--Finding 3rd Maximum salary from Employee table using SELECT query
select * from Employee order by salary desc
--Using Subquery
SELECT Salary,EmployeeName, EmployeeId
FROM(SELECT Salary,EmployeeName, EmployeeId,ROW_NUMBER() OVER(ORDER BY Salary desc) As RowNum FROM EMPLOYEE) As A 
WHERE A.RowNum IN (3,3);
-----------------------------------------------------------------------------------------------------------------------
SELECT Employeename,EmployeeId, Salary FROM Employee e1 WHERE 3-1 = (SELECT COUNT(DISTINCT salary) FROM Employee e2
WHERE e2.salary > e1.salary);
-----------------------------------------------------------------------------------------------------------------------
--Using CTE
WITH CTE AS (SELECT Employeename, EmployeeId, Salary, RN = ROW_NUMBER() OVER (ORDER BY Salary DESC)FROM dbo.Employee)
SELECT Employeename, EmployeeId, Salary FROM CTE WHERE RN = 3;
-----------------------------------------------------------------------------------------------------------------------
--ALTER TABLE basics � Alter a column datatype from varchar to nvarchar using ALTER statement
ALTER TABLE Location ALTER COLUMN Address nvarchar(max);
--UPDATE query basics � Update Employee table by setting Employee Status =�inactive� for employees age above 60
UPDATE Employee SET IsActive =0 where AGE>60
SELECT * FROM Employee
--DELETE query basics � Delete from employee table where Employee status = inactive� 
--and not accessed for last 90 days
SELECT * FROM Employee
--print 'Row with Id 21 will be deleted'
Delete from Employee where Age>60 and LastLogin< (SELECT DATEADD(DD,-90,SYSDATETIME()))


